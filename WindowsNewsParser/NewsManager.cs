﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using System.Windows.Controls;

namespace WindowsNewsParser
{
    class Source
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    class Article
    {
        public Source source { get; set; }
        public string author { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public string urlToImage { get; set; }
        public DateTime publishedAt { get; set; }
        public string content { get; set; }
    }

    class News
    {
        public string status { get; set; }
        public int totalResults { get; set; }
        public IList<Article> articles { get; set; }
    }

    class NewsManager
    {
        private static NewsManager instance;
        private static readonly string source = "https://newsapi.org/v2/top-headlines?";
        private static readonly string apiKey = "cb1997d05af0435eaf431a5b8907a8df";
        private static readonly string country = "us";
        private static readonly string searchSource = "https://newsapi.org/v2/everything?";

        private NewsManager()
        { }

        public static NewsManager GetInstance()
        {
            if (instance == null)
                instance = new NewsManager();
            return instance;
        }

        public IList<Article> LoadNews()
        {
            string url = $"{source}{$"country={country}&"}{$"apiKey={apiKey}"}";

            string json = new WebClient() { Encoding = Encoding.UTF8 }.DownloadString(url);
            News news = JsonConvert.DeserializeObject<News>(json);
            return news.articles;
        }

        public IList<Article> FindUSNews(string quotation)
        {
            IList<Article> articles = LoadNews();
            var filter = articles.Where(x =>
            {
                if (x.description == null)
                {
                    return x.title.ToLower().Contains(quotation);
                }
                else
                {
                    return x.title.ToLower().Contains(quotation) ||
                x.description.ToLower().Contains(quotation);
                }
            });
            return filter.ToList();
        }

        public IList<Article> FindNews(string quotation)
        {
            //in case of API plan restrictions we must use only current date
            var year = DateTime.Now.Year.ToString();
            var month = DateTime.Now.Month.ToString();
            var day = DateTime.Now.Day.ToString();
            var url = searchSource +
          $"q={quotation}&" +
          $"from={year}-{month}-{day}&" +
          "sortBy=popularity&" +
          $"apiKey={apiKey}";
            string json = new WebClient() { Encoding = Encoding.UTF8 }.DownloadString(url);
            News news = JsonConvert.DeserializeObject<News>(json);
            
            return news.articles;
        }
               
    }

}
