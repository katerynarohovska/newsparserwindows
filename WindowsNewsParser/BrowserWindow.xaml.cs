﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using mshtml;
namespace WindowsNewsParser
{
    /// <summary>
    /// Логика взаимодействия для BrowserWindow.xaml
    /// </summary>
    public partial class BrowserWindow : Window
    {
        bool isLoaded;

        public BrowserWindow()
        {
            InitializeComponent();

        }
        public BrowserWindow(string url)
        {
            InitializeComponent();
            webBrowser.Navigate(url);
            isLoaded = false;
            // to supress JS warnings
            dynamic activeX = this.webBrowser.GetType().InvokeMember("ActiveXInstance",
                BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, this.webBrowser, new object[] { });

            activeX.Silent = true;

        }

        private void WebBrowser_Loaded(object sender, RoutedEventArgs e)
        {
            isLoaded = true;
        }

        private void WebBrowser_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (isLoaded)
            {
                e.Cancel = true;
            }
        }

        private void WebBrowser_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (isLoaded)
            {
                e.Handled = true;
            }
        }

       
    }
}
