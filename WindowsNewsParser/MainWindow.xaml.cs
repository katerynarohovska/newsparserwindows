﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WindowsNewsParser
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private IList<Article> currentNews;
        private Boolean isUSOnly;

        public MainWindow()
        {
            InitializeComponent();
            currentNews = NewsManager.GetInstance().LoadNews();
            ResizeMode = ResizeMode.NoResize;

            isUSOnly = true;        
            ReloadList();
            
            
        }

        private void ReloadList()
        {
            newsList.Items.Clear();

            for (int i = 0; i < currentNews.Count; i++)
            {
               newsList.Items.Add(currentNews[i]);
            }

        }
        
        private void NewsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (newsList.SelectedIndex == -1)
            {
                return;
            }
            string url = currentNews[newsList.SelectedIndex].url;
            BrowserWindow browser = new BrowserWindow(url);
            browser.Title = currentNews[newsList.SelectedIndex].title;
            browser.Show();
        }

        private void SearchModeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (searchModeComboBox.SelectedIndex == -1) {
                return;
            }
            if (searchModeComboBox.SelectedIndex == 0) {
                isUSOnly = true;
            } else
            {
                isUSOnly = false;
            }
        }

        private void Search()
        {
            if (searchField.Text.Length == 0) { return; }
            string searchText = searchField.Text.ToLower();
            if (isUSOnly)
            {
                currentNews = NewsManager.GetInstance().FindUSNews(searchText);
            }
            else
            {
                currentNews = NewsManager.GetInstance().FindNews(searchText);
            }
            ReloadList();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void SearchField_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Search();
            }
        }
    }
}
